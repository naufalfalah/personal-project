<?php

namespace App\Controllers;

class Pages extends BaseController
{
	public function index()
	{
		$data = [
			'title' => 'Home'
		];
		return view('pages/home', $data);
	}

	public function about()
	{
		$data = [
			'title' => 'About Me'
		];
		return view('pages/about', $data);
	}

	public function contact()
	{
		$data = [
			'title' => 'Contact Us',
			'alamat' => [
				[
					'tipe' => 'Rumah',
					'alamat' => 'Jl. abc No. 123',
					'kota' => 'Depok'
				],
				[
					'tipe' => 'Kantor',
					'alamat' => 'Jl. Melati 3 No. 193',
					'kota' => 'Depok'
				]
			]
		];
		return view('pages/contact', $data);
	}
}
